CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Maintainers


 INTRODUCTION
------------

Body Class is a simple module that allows users to add custom CSS classes
to any node through the node/add interface. The respective classes will be
appearing in body tags

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

MAINTAINERS
-----------

Current maintainers:
 * Jaseer Kinangattil (JaseerKinangattil)
   - https://www.drupal.org/u/jaseerkinangattil
